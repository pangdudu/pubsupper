#! /usr/bin/ruby
require "rubygems"
require "xmpp4r"

require "xmpp4r/pubsub"
require "xmpp4r/pubsub/helper/servicehelper.rb"
require "xmpp4r/pubsub/helper/nodebrowser.rb"
require "xmpp4r/pubsub/helper/nodehelper.rb"
include Jabber
Jabber::debug = false

class NodeCreator

  def initialize
    @service = 'pubsub.ami.www.techfak.uni-bielefeld.de'
    jid = 'ping@ami.www.techfak.uni-bielefeld.de/XMPP4R'
    password = 'ping'
    @client = Client.new(JID.new(jid))
    @client.connect
    @client.auth(password)
  end

  def createnodes
    @client.send(Jabber::Presence.new.set_type(:available))
    pubsub = PubSub::ServiceHelper.new(@client, @service)
    pubsub.create_node('home/ami.www.techfak.uni-bielefeld.de/ping/')
    pubsub.create_node('home/ami.www.techfak.uni-bielefeld.de/ping/updates')
  end

end

nc = NodeCreator
