#! /usr/bin/ruby
require "rubygems"

require "xmpp4r"
require "xmpp4r/pubsub"
require "xmpp4r/pubsub/helper/servicehelper.rb"
require "xmpp4r/pubsub/helper/nodebrowser.rb"
require "xmpp4r/pubsub/helper/nodehelper.rb"
#require "unprof"
include Jabber
Jabber::debug = false

class Publisher

  def initialize id
    @id = id
    @username = 'ping'
    @host = 'ami.www.techfak.uni-bielefeld.de'
    @jid = "#{@username}@#{@host}/#{id}"
    @password = @username #this is not allways true
    @service = "pubsub.#{@host}"
    @node = "home/#{@host}/#{@username}/updates"
    @time = Time.now
    @counter = 0
    puts "node: #{@node}"
  end

  def connect
    puts "Trying to connect thread: #{@id}!"
    # connect XMPP client
    @client = Client.new(JID.new(@jid))
    @client.connect(@hostname)
    @client.auth(@password)
    @client.send(Jabber::Presence.new.set_type(:available))
    puts "I am connected, great success!"
    #ok, this will be blocking    
    loop do 
      createitem 'I like!'
    end
  end

  def createitem text
    # create item
    pubsub = PubSub::ServiceHelper.new(@client, @service)
    item = Jabber::PubSub::Item.new
    xml = REXML::Element.new("Timestamp")
    xml.text = text
    item.add(xml)
    #publish item
    pubsub.publish_item_to(@node, item)
    fpscount 
  end

  def fpscount
    @counter += 1
    if @counter >= 100
      @counter = 0
      timedelta = Time.now - @time      
      @time = Time.now
      puts "ID: #{@id} FPS: #{100/timedelta}"
    end  
  end

end

threads = []

(1..3).each do |i|
  puppy = Publisher.new i
  threads[i] = Thread.new do
    puppy.connect
  end
end

threads[1].join
